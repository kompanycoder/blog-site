// require dependancies for the app
const express = require("express");
const bodyParser = require("body-parser");
const ejs = require("ejs");
const mongoose = require("mongoose");
const morgan = require("morgan");
const cors = require("cors");
const session = require("express-session");

// get env function from config
const config = require("./config/config").get(process.env.NODE_ENV);
// console.log(config);
// express app
const app = express();
// include router files
const generalRoutes = require("./routes/generalRoutes");
const postRoutes = require("./routes/postRoutes");

app.set("view engine", ejs);
app.use(morgan("dev"));
app.use(cors());
app.use(
  session({
    secret: config.SECRETKEY,
    resave: true,
    saveUninitialized: true,
    name: "Modern Blog - Future-Lens"
  })
);
app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// use express with the custom route
app.use("/", generalRoutes);
app.use("/api/", postRoutes);

// add connection to db
const mongoOptions = { useNewUrlParser: true };

// mongoose global promise
mongoose.Promise = global.Promise;
mongoose.connect(config.MONGOURI, mongoOptions, (err,next) => {
  if (err) {
    return next(err);
  } else {
    console.log("Connected to blogDb...");
  }
});

// init app listening port
app.listen(config.APPPORT, () => {
  console.log("Blog app is restarted on port: " + config.APPPORT);
});
