// require mongoose
const mongoose = require("mongoose");

// create mongoose schema
const PostSchema = mongoose.Schema({
  title: {
    type: String,
    max: 50,
    required: true
  },
  content: {
    type: String,
    max: 500,
    required: true
  },
  img: {
    type: String
  },
  replies: ["string"]
});

// method for the save
PostSchema.pre("save", next => {
  let post = this;
  post.save((err, doc) => {
    if (err) return next(err);
    next(null, doc);
  });
});

// model new posts based on the schema
const Post = mongoose.model("Post", PostSchema);

module.exports = {
  Post
};
