"use strict";
const config = {
    production:{
        MONGOURI:process.env.mongoUri || 'online mongoDb',
        SECRETKEY:'TOPSECRETKEY12345',
        APPPORT: process.env.PORT
    },
    development:{
        MONGOURI:  'mongodb://localhost:27017/blogDb',
        SECRETKEY:'TOPSECRETKEY12345',
        APPPORT: 3000

    }

};

exports.get = (env) => {
    return config[env] || config.development;
}
