const router = require("express").Router();
const { Post } = require("../models/Post");

//get all posts route= working
router.get("/posts", (req, res, next) => {
  Post.find((err, posts) => {
    if (err) return next(err);
    return res.render("_pages/post/posts.ejs", {
      posts
    });
  });
});

// get route to display form 
router.get("/new", (req,res)=>{
  return res.render("_pages/post/newPost.ejs");
})

// add post from the user form and add to the db
router.post("/new", (req, res, next) => {
  
  // add new post to db
  const newPost = new Post({
    title: req.body.title,
    content: req.body.content
  });
  
  newPost.save((err, savedPost)=>{
    if(err) return next(err);
    return res.status(200).json({
      saved: true,
      doc: savedPost
    });
  });
  res.redirect('/api/posts');
});

// single post route
router.get("/single-post", (req, res, next) => {
  let id = req.query.id;

  Post.findById({ _id: id }, (err, post) => {
    if (err) return next(err);
    return res.render("_pages/post/singlePost.ejs", {
      post
    });
  });
});
// get single post  and delete route
router.delete("/single-post/delete-post", (req, res) => {
  let post = req.body;

  res.send("post deleted: " + post);
});

module.exports = router;
