const router = require("express").Router();

// static asset to be loaded
const contentForHome = "Mordern Blog on everything Technology";
const aboutRender = "This is a blog website for keeping records of every day coding activity for the fullstack developer.";
const aboutSecond = "It was build using ejs templating engine, express, node, bodyparser,bootstrap and mongoose for the database";

//  home route
router.get("/", (req, res) => {
  res.render("_pages/home.ejs", {
    main: contentForHome
  });
});

// about page route
router.get("/about", (req, res) => {
  res.render("_pages/about.ejs", {
    StartAbout: aboutRender,
    SecondAbout: aboutSecond
  });
});

// contact page route
router.get("/contact", (req, res) => {
  res.render("_pages/contact.ejs");
});

module.exports = router;
